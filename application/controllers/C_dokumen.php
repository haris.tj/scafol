<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_dokumen extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('M_dokumen');	
	}

	function index(){
		$this->load->view('register');
	}

	function aksi_register(){
		$alamat = $this->input->post('alamat');
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$data = array(
			'alamat' => $alamat,
			'email' => $email,
			'password' => $password);

		$this->M_login->register($data);
		$this->session->set_flashdata('ok','Data Telah Ditambahkan');
		redirect(base_url('C_login'));

	}


// 
	function insert(){
		$count = count($_FILES['files']['name']);
		for($i=0;$i<$count;$i++){
			
			if(!empty($_FILES['files']['name'][$i])){
				
				$_FILES['file']['name'] = $_FILES['files']['name'][$i];
				$_FILES['file']['type'] = $_FILES['files']['type'][$i];
				$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
				$_FILES['file']['error'] = $_FILES['files']['error'][$i];
				$_FILES['file']['size'] = $_FILES['files']['size'][$i];
				
				$config['upload_path'] = 'uploads/'; 
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['max_size'] = '5000';
				
				$this->load->library('upload',$config); 
				
				if($this->upload->do_upload('file')){
					$uploadData = $this->upload->data();
					$filename = $uploadData['file_name'];

					$data = array(
						'nama_doc' => $uploadData['file_name'],
						'tgl_upload' => date('d M Y'),
						'pengapload' => 'mass haris'
					);
					$this->db->insert('tb_dokumen',$data);

				}
			}
			
		}
		echo count($data);
	}
	//BATAS AKHIR 
	function test(){

		$config['upload_path']   = FCPATH.'uploads/';
		$config['allowed_types'] = 'gif|jpg|png|ico|jpeg';
		$this->load->library('upload',$config);

		if($this->upload->do_upload('userfile')){
			$data = array(
				'nama_doc' => $this->upload->data('file_name'),
				'tgl_upload' => date('d M Y'),
				'pengapload' => 'mass haris'
			);
			$this->db->insert('tb_dokumen',$data);
		}
	}
	function delete(){

		//Ambil token foto
		$id=$this->input->post('id_doc');

		
		$file=$this->db->get_where('tb_dokumen',array('id_doc'=>$id));


		if($file->num_rows()>0){
			$hasil=$file->row();
			$nama_file=$hasil->nama_doc;
			if(file_exists($file=FCPATH.'uploads/'.$nama_file)){
				unlink($file);
			}
			$this->db->delete('tb_dokumen',array('id_doc'=>$id));

		}
		echo "{}";
	}

	function viewdiskusi(){
		$data = array(
			'data'	=> $this->M_dokumen->list_disk()
		);
		$this->load->view('diskusi',$data);
	}

	function insertdiskusi(){
		$data = array(
			'nama' => 'Mass Haris',
			'tgl' => date('d M Y'),
			'judul'	=> $this->input->post('judul'),
			'deskripsi'	=> $this->input->post('deskripsi'),
			'kategori'	=> $this->input->post('kategori'),
		);
		$this->db->insert('tb_diskusi',$data);
	}

	function insertkomentar(){
		$data = array(
			'id_diskusi' => $this->input->post('id_diskusi'), 
			'nama' => 'user'.random_int(1, 100),
			'tgl' => date('d M Y'),
			'komentar'	=> $this->input->post('komentar')
		);
		$this->db->insert('tb_komentar',$data);
	}

	function show_komen($id){
		$data = $this->M_dokumen->s_komen($id);
		echo json_encode($data);
	}
}
?>