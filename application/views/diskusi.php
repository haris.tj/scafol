<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <title>Home</title>
</head>
<body>
  <main class="container">
    <br>
    <div class="card">
      <div class="card-header">
        <div class="float-left">
          <button class="btn btn-warning text-light" id="tambah" data-toggle="modal" data-target="#exampleModalCenter">Tambah Diskusi</button>
        </div>
        <div class="float-right">
          <div class="row form-group">
            <div class="col-sm-6">
              <input type="text" name="Cari" placeholder="Cari" name="cari" class="form-control">
            </div>
            <div class="col-sm-6">
              <select class="form-control">
                <option>Filter By</option>
                <option value="id">ID</option>
                <option value="name">Name</option>
              </select>
            </div>
          </div>
        </div>
      </div>
      <!-- end card header -->
      <div class="card-body">
        <div id="accordion">

          <!-- baggian 1 -->
          <?php foreach ($data as $dt) {?>
            <div class="card">
              <div class="card-body" id="heading<?= $dt->id_diskusi ?>">

                <div class="row form-group">
                  <div class="col-md-1">
                    <div class="btn btn-link collapsed" data-toggle="collapse" data-target="#collaps<?= $dt->id_diskusi ?>" aria-expanded="false" aria-controls="collaps<?= $dt->id_diskusi ?>" data-id="<?= $dt->id_diskusi?>">
                      <img src="<?php echo base_url("img/img.jpg");?>" class="rounded-circle border-warning" width="60" height="60">
                    </div>
                  </div>
                  <div class="col-md-10">
                    <small><?= $dt->nama ?> <span class="text-warning">(<?= $dt->tgl ?>)</span></small><br>
                    <p><?= $dt->judul ?></p>


                    <!-- YG TERSEMBUNYI -->
                    <div id="collaps<?= $dt->id_diskusi ?>" class="collapse" aria-labelledby="heading<?= $dt->id_diskusi ?>" data-parent="#accordion">
                      <?= $dt->deskripsi ?>
                      <p>
                        <h6 id="total<?= $dt->id_diskusi ?>"></h6>
                        <form method="POST">
                          <input type="hidden" id="id_diskusi" name="id_diskusi" value="<?= $dt->id_diskusi?>">
                          <textarea class="form-control" rows="5" placeholder="Tulis Komentar Anda Disini" name="komen<?= $dt->id_diskusi ?>" id="komen<?= $dt->id_diskusi ?>" required></textarea>
                        </form>
                        <div class="float-right">
                          <br>
                          <button type="submit" id="simpan<?= $dt->id_diskusi?>" value="<?= $dt->id_diskusi?>" class="btn btn-sm btn-warning text-light smp">Kirim</button>
                        </div>
                      </p>
                      <!-- new row komenttar -->

                      <div id="tampil-komen<?= $dt->id_diskusi?>">

                      </div>
                      <!-- batas row komenttar -->
                    </div>
                    <!-- BATAS YG TERSEMBUNYI -->


                  </div>
                  <div class="col-md-1">
                    <i class="fa fa-envelope fa-lg text-warning" aria-hidden="true"></i>
                    <!-- <span class="font-weight-bold">36</span> -->
                  </div>
                </div>
              </div>
            </div>
            <br>
          <?php } ?>
          <!-- bagian 1 -->
          <!-- BATAS -->
        </div>
      </div>
      <!-- END CARD-BODY -->

    </div>
  </main>

  <!-- MODAL START -->
  <!-- Modal -->
  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" id="modal">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Tambah Diskusi</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="container-fluid">
            <form method="POST">
              <div class="row form-group">
                <input type="text" name="judul" id="judul" class="form-control" placeholder="Tulis Judul Deskripsi Disini.." required>
              </div>
              <div class="row form-group">
                <textarea rows="5" class="form-control" id="deskripsi" name="deskripsi" required placeholder="Deskripsi Disini"></textarea>
              </div>
              <div class="row form-group">
                <select class="form-control" name="kategori" id="kategori" required>
                  <option value="kategori">Kategori</option>
                  <option value="Jembatan">Jembatan</option>
                </select>
              </div>
            </form>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-sm btn-warning text-light" id="simpan">Simpan</button>
        </div>
      </div>
    </div>
  </div>
  <!-- MODAL END -->
</body>
<!-- Optional JavaScript -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script> -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> 
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->

<!-- <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script> -->


<script type="text/javascript">
  $(document).ready(function() {
    show_data();
    $('.smp').click(function(event) {

      var id = $(this).val();
      var komen = $('#komen'+id).val();

      $.ajax({
        url: '<?php echo base_url("C_dokumen/insertkomentar");?>',
        type: 'POST',
        data: { komentar: komen, id_diskusi: id },
        success: function(dataResult){
          alert('insert success');
          window.location.reload();
        }
      });
    });

    function show_data(){
      $('.collapsed').click(function(event) {
        var id = $(this).data('id');
        var url = '<?php echo base_url("C_dokumen/show_komen/");?>'+id;
        // CKEDITOR.replace('komen'+id);
        $.ajax({
          url: url,
          type  : 'ajax',
          async : false,
          dataType : 'json',
          success : function(data){
            $('#total'+id).html(data.length+' Jawaban')
            var html = '';
            var i;
            for(i=0; i<data.length; i++){
              html += 
              '<div class="row form-group">'+
              '<div class="col-sm-1">'+
              '<br>'+
              '<img src="<?php echo base_url("img/img.jpg");?>" class="rounded-circle border-warning" width="60" height="60">'+
              '</div>'+
              '<div class="col-sm-10">'+
              '<hr>'+
              '<small>'+data[i].nama+' <span class="text-warning"> ('+data[i].tgl+')</span></small><br>'+
              '<p>'+data[i].komentar+'</p>'+
              '</div>'+
              '<div class="col-sm-1"></div>'+
              '<div class="col-sm-12"></div>'+
              '</div>';
            }
            $('#tampil-komen'+id).html(html);
          }

        });
      });
    }
    // insert
    $('#simpan').click(function(event) {
      /* Act on the event */
      var judul = $('#judul').val();
      var deskripsi = $('#deskripsi').val();
      var kategori = $('#kategori').val();

      $.ajax({
        url: '<?php echo base_url("C_dokumen/insertdiskusi");?>',
        type: 'POST',
        data: {
          judul: judul,
          deskripsi: deskripsi,
          kategori: kategori
        },
        success: function(dataResult){
          alert('insert success');
          window.location.reload();
        }
      });
    });
    // end insert
  });
</script>
</body>
</html>