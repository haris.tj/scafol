 <div class="card shadow mb-4">
 	<div class="card-header py-3">
 		<h6 class="m-0 font-weight-bold text-primary">Lengkapi Riwayat</h6>
 	</div>
 	<div class="card-body">
 		<form method="POST" action="<?php echo base_url('C_user/insert_riwayat') ?>">
 			<h6 class="m-0 font-weight-bold text-primary">Pendidikan</h6>
 			<div class="form-row">
 				<div class="form-group col-md-6">
 					<label for="inputEmail4">Nama Institusi Akademik</label>
 					<input type="text" class="form-control" id="akademik" placeholder="Universitas XYZ" name="akademik" required>
 				</div>
 				<div class="form-group col-md-6">
 					<label for="inputEmail4">Jurusan</label>
 					<input type="text" class="form-control" id="jurusan" placeholder="IT" name="jurusan" required>
 				</div>
 				<div class="form-group col-md-4">
 					<label for="inputEmail4">Jenjang Pendidikan Terakir</label>
 					<input type="text" class="form-control" id="jpt" placeholder="S1" name="jpt" required>
 				</div>
 				<div class="form-group col-md-4">
 					<label for="inputEmail4">Tahun Lulus</label>
 					<input type="number" class="form-control" id="thn_lulus" placeholder="2020" name="thn_lulus" required>
 				</div>
 				<div class="form-group col-md-4">
 					<label for="inputEmail4">IPK</label>
 					<input type="number" class="form-control" id="ipk" placeholder="3.99" name="ipk" required>
 				</div>
 			</div>
 			<hr class="sidebar-divider my-0"><br>
 			<h6 class="m-0 font-weight-bold text-primary">Kursus / Seminar</h6>
 			<div class="form-row">
 				<div class="form-group col-md-4">
 					<label for="inputEmail4">Nama Kursus / Seminar</label>
 					<input type="text" class="form-control" id="kursus" placeholder="Kursus XYZ" name="kursus" required>
 				</div>
 				<div class="form-group col-md-4">
 					<label for="inputEmail4">Sertifikat</label>
 					<select id="inputState" class="form-control" name="sertifikat" id="sertifikat" required>
 						<option selected>Choose...</option>
 						<option value="Ada">Ada</option>
 						<option value="Tidak Ada">Tidak Ada</option>
 					</select>
 				</div>
 				<div class="form-group col-md-4">
 					<label for="inputEmail4">Tahun</label>
 					<input type="number" class="form-control" id="tahun_1" placeholder="2020" name="tahun_1" required>
 				</div>
 			</div>
 			<hr class="sidebar-divider my-0"><br>
 			<h6 class="m-0 font-weight-bold text-primary">Riwayat Pekerjaan</h6>
 			<div class="form-row">
 				<div class="form-group col-md-3">
 					<label for="inputEmail4">Nama Perusahaan</label>
 					<input type="text" class="form-control" id="perusaahan" placeholder="Perusahaan XYZ" name="perusaahan" required>
 				</div>
 				<div class="form-group col-md-3">
 					<label for="inputEmail4">Posisi Terakir</label>
 					<input type="text" class="form-control" id="posisi" placeholder="Member IT Security" name="posisi" required>
 				</div>
 				<div class="form-group col-md-3">
 					<label for="inputEmail4">Pendapatan</label>
 					<input type="number" class="form-control" id="pendapatan" placeholder="5.000.000" name="pendapatan" required>
 				</div>
 				<div class="form-group col-md-3">
 					<label for="inputEmail4">Tahun</label>
 					<input type="number" class="form-control" id="tahun_2" placeholder="2020" name="tahun_2" required>
 				</div>
 			</div>
 			<div class="form-row">
 				<div class="form-group col-md-9">
 					<label for="inputEmail4">Skill</label>
 					<input type="text" class="form-control" id="skill" placeholder="PHP, Css, HTML" name="skill" required>
 				</div>
 				
 				<div class="form-group col-md-3">
 					<label for="inputEmail4">Gaji Diharapkan</label>
 					<input type="number" class="form-control" id="hasil" placeholder="6.000.000" name="hasil" required>
 				</div>
 			</div>
 			<hr class="sidebar-divider my-0"><br>
 			<button type="submit" class="btn btn-primary" id="simpan">Save</button>
 		</form>
 	</div>
 </div>