 <div class="card shadow mb-4">
 	<div class="card-header py-3">
 		<h6 class="m-0 font-weight-bold text-primary">Lengkapi Biodata</h6>
 	</div>
 	<div class="card-body">
 		<form method="POST" action="<?php echo base_url('C_user/insert_action') ?>">
 			<div class="form-row">
 				<div class="form-group col-md-4">
 					<label for="inputEmail4">Posisi Yang Dilamar</label>
 					<input type="text" class="form-control" id="posisi" placeholder="Posisi Lamaran" name="posisi" required>
 				</div>
 				<div class="form-group col-md-4">
 					<label for="inputEmail4">Nama</label>
 					<input type="text" class="form-control" id="nama" placeholder="Nama Lengkap" name="nama" required>
 				</div>
 				<div class="form-group col-md-4">
 					<label for="inputEmail4">No KTP</label>
 					<input type="number" class="form-control" id="noktp" placeholder="53123040590003" name="nokpt" required>
 				</div>
 				<div class="form-group col-md-6">
 					<label for="inputEmail4">Tempat TGL Lahir</label>
 					<input type="text" class="form-control" id="ttl" placeholder="Kupang, 12-12-2020" name="ttl" required>
 				</div>
 				<div class="form-group col-md-3">
 					<label for="inputEmail4">Jenis Kelamin</label>
 					<select id="inputState" class="form-control" name="jk" id="jk" required>
 						<option selected>Choose...</option>
 						<option value="Laki-Laki">Laki-Laki</option>
 						<option value="Perempuan">Perempuan</option>
 					</select>
 				</div>
 				<div class="form-group col-md-3">
 					<label for="inputEmail4">Agama</label>
 					<input type="text" class="form-control" id="agama" placeholder="Kristen" name="agama" required>
 				</div>
 				<div class="form-group col-md-6">
 					<label for="inputPassword4">Gol Darah</label>
 					<input type="text" class="form-control" id="gold" placeholder="A" name="gold" required>
 				</div>
 				<div class="form-group col-md-6">
 					<label for="inputPassword4">Status</label>
 					<input type="text" class="form-control" id="status" placeholder="Menikah / Belum Menikah" name="status" required>
 				</div>
 				<div class="form-group col-md-6">
 					<label for="inputPassword4">Alamat KTP</label>
 					<input type="text" class="form-control" id="a_ktp" placeholder="Perumsat Ki penjawi Uksw no 9" name="a_ktp" required>
 				</div>
 				<div class="form-group col-md-6">
 					<label for="inputPassword4">Alamat Tinggal</label>
 					<input type="text" class="form-control" id="a_tgl" placeholder="Perumsat Ki penjawi Uksw no 9" name="a_tgl" required>
 				</div>
 				<div class="form-group col-md-4">
 					<label for="inputPassword4">Email</label>
 					<input type="email" class="form-control" id="email" placeholder="mail@mail.com" name="email" required>
 				</div>
 				<div class="form-group col-md-4">
 					<label for="inputPassword4">No Telepon</label>
 					<input type="number" class="form-control" id="tlpn" placeholder="081222765555" name="tlpn" required>
 				</div>
 				<div class="form-group col-md-4">
 					<label for="inputPassword4">TLP Orang Terdekat</label>
 					<input type="number" class="form-control" id="o_tlp" placeholder="081222765555" name="o_tlp" required>
 				</div>
 			</div>
 			<button type="submit" class="btn btn-primary" id="simpan">Save</button>
 		</form>
 	</div>
 </div>