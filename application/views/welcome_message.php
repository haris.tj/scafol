<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> -->
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css">
	<link rel="stylesheet" href="dropzone/dist/dropzone.css">
	<!-- <link rel="stylesheet" href="node_modules/dm-file-uploader/dist/css/jquery.dm-uploader.min.css"> -->

	<title>Home</title>
</head>
<body>
	<div class="container">
		<div class="card">
			<div class="card-header">
				<button class="btn-sm btn btn-warning text-light" data-toggle="modal" data-target=".bd-example-modal-lg">Tambah Laporan</button>
				<button class="btn btn-sm btn-warning text-light">Download Laporan</button>
			</div>
			<div class="card-body">
				<div class="card-text">
					<table id="example" class="table table-striped table-bordered" style="width:100%">
						<thead>
							<tr>
								<th>#</th>
								<th>Nama Dokumen</th>
								<th>Tanggal Upload</th>
								<th>Pengapload</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php for($x=1; $x<20; $x++){ ?>
								<tr>
									<td><?= $x ?></td>
									<td>Dokumen <?= $x ?></td>
									<td><?= date('d M Y')?></td>
									<td>Mass Haris</td>
									<td>
										<button class="btn btn-sm btn-danger" id="hapus">Hapus</button>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!--START MODAL -->
	<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Tambah DED</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="dropzone" id="dropzone">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!--END MODAL  -->
<!-- Optional JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!-- <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script> -->
<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script> -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script> -->
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> -->
<!-- <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script> -->
<!-- <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script> -->
<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
<!-- <script src="node_modules/dm-file-uploader/dist/js/jquery.dm-uploader.min.js"></script> -->
<!-- <script src="node_modules/dm-file-uploader/dist/js/demo-config.js"></script> -->
<!-- <script src="node_modules/dm-file-uploader/dist/js/demo-ui.js"></script> -->
<script src="dropzone/dist/dropzone.js"></script>
<script type="text/javascript">
	Dropzone.autoDiscover = false;

	var foto_upload= new Dropzone(".dropzone",{
		url: "<?php echo base_url('C_dokumen/test') ?>",
		maxFilesize: 2,
		method:"post",
		acceptedFiles:"image/*",
		paramName:"userfile",
		dictInvalidFileType:"Type file ini tidak dizinkan",
		addRemoveLinks:true,
		success: function (file, response) {
			if ($('#modal').modal('hide')) {}
				file.previewElement.outerHTML = "";
		}
	});

	foto_upload.on("removedfile",function(a){
		var id_doc=a.id_doc;
		$.ajax({
			type:"post",
			data:{id_doc:id_doc},
			url:"<?php echo base_url('C_dokumen/delete') ?>",
			cache:false,
			dataType: 'json',
			success: function(){
				console.log("File terhapus");
			},
			error: function(){
				console.log("Error");
			}
		});
	});
</script>
</body>
</html>