<?php
class M_dokumen extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function cek_login($email,$password){
		$this->db->where('email',$email);
		$this->db->where('password',$password);
		$result = $this->db->get('tb_user',1);
		return $result;
	}
	function insert($data) {
		$this->db->insert('tb_dokumen',$data);
	}
	function list_disk(){
		$hasil=$this->db->query("SELECT * FROM tb_diskusi");
		return $hasil->result();
	}
	function count() {
		$this->db->select('*');
		$this->db->from('news_data');
		$id = $this->db->get()->num_rows();
		return $id;
	}
	function list_komen(){
		$hasil=$this->db->query("SELECT * FROM tb_komentar");
		return $hasil->result();
	}
	function s_komen($id){
		$this->db->where('id_diskusi', $id);
		$data = $this->db->get('tb_komentar');
		return $data->result();
	}
}
?>